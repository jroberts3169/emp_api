const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// API file for interacting with MariaDB
const api = require('./api');

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// API location
app.use('/api', api);

//Set Port
const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Service is running on localhost:${PORT}`);
});
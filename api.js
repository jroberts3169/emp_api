const express = require('express');
const app = express();
const connection = require('./mysql');
var cors = require('cors');
var sizeof = require('sizeof'); 
var moment = require('moment');

app.use(cors());

// endpoint to retrieve list of backup data
app.get('/files', (req, res)=> {
    connection.query("SELECT * FROM system1", (err, rows) => {
        if(err) throw err;
        res.send(rows);
    })
});

// endpoint for sending the backup data
app.post('/upload', (req, res) => {
    console.log("File received at: " + new Date().toGMTString());
    var data = req.body.data;
    var fileSize = sizeof.sizeof(req.body.data);
    var fileName = req.body.fileName;
    var transmitTime = req.body.transmitTime;
    var receiveTime = moment().format('YYYY-DD-MM HH:mm:ss:SSS');
    var query = "INSERT INTO `system1` (fileName, fileSize, data, transmitTime, receiveTime) VALUES (?, ?, ?, ?, ?)";

    connection.query(query, [fileName, fileSize, data, transmitTime, receiveTime], (err) => {
        if(err) throw err;
        res.send({"msg": "Data has been received"});
    });
})

module.exports = app;
